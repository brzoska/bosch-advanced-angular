import { Component } from '@angular/core';

@Component({
  selector: 'bsh-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'bosch';
}
