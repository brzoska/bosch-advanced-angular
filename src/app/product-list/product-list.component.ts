import { Component, OnInit } from '@angular/core';
import { Product } from '../models/product';

@Component({
  selector: 'bsh-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit {

  product: Product = {
    price: 1.99,
    id: '1',
    description: 'Product 1 description',
    title: 'Product 1'
  };
  constructor() { }

  ngOnInit() {
  }

  onSprzedaj(product: Product) {
    alert(product);
  }
}
