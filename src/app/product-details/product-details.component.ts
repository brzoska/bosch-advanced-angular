import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Product } from '../models/product';

@Component({
  selector: 'bsh-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.scss']
})
export class ProductDetailsComponent implements OnInit {

  @Input() product: Product;
  @Output() sprzedaj = new EventEmitter<string>();
  constructor() { }

  ngOnInit() {
  }
  handleSprzedaj() {
    this.sprzedaj.emit(this.product.title.toUpperCase())
  }

}
